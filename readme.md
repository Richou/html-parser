# Html Parser

A simple HTML Parser

## Prerequisites

 - nvm, node version manager

## Before launch

*Use the node recommanded version*

```bash
$ nvm use
```

This will install and define the correct node version

*Create a .env file*

```
APPLICATION_LOG_LEVEL=debug|info
```

## Launch

The application will find the file test.html in the assets directory for processing

```bash
$ node index.js
```

At the end of the process the application generated a file named ```test-result.json``` with the generated JSON.