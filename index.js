require('babel-polyfill');
require( 'babel-core/register' );
require('dotenv').config();
const HtmlParser = require('./src/html.parser');

const htmlParser = new HtmlParser('./assets');
htmlParser.process();