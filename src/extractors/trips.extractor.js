import url from 'url';
import querystring from 'querystring';

export class TripsExtractor {
    constructor($, datesExtractor) {
        this.$ = $;
        this.datesExtractor = datesExtractor;
    }

    extract() {
        const aftersaleUrl = this.$('#aftersale-info').find('a.more-info').attr('href');
        const parsedUrl = url.parse(aftersaleUrl);
        const parsedQuery = querystring.parse(parsedUrl.query);
        const departuresHour = this.$('#block-command').find('.origin-destination-hour.segment-departure').get().map(item => item.children[0].data.trim().replace('h', ':'));
        const departuresStation = this.$('#block-command').find('.origin-destination-station.segment-departure').get().map(item => item.children[0].data.trim().replace('h', ':'));
        const arrivalsHour = this.$('#block-command').find('.origin-destination-hour.segment-arrival').get().map(item => item.children[0].data.trim().replace('h', ':'));
        const arrivalsStation = this.$('#block-command').find('.origin-destination-station.segment-arrival').get().map(item => item.children[0].data.trim().replace('h', ':'));
        const trainTypes = this.$('#block-command').find('.product-details td:nth-child(4)').get().map(it => it.children[0].data.trim());
        const trainNumbers = this.$('#block-command').find('.product-details td:nth-child(5)').get().map(it => it.children[0].data.trim());
        const trains = [];
        if (departuresHour.length === departuresStation.length && departuresStation.length === arrivalsHour.length && arrivalsHour.length === arrivalsStation.length) {
            for(let index = 0; index < departuresHour.length; index++) {
                trains.push(Object.assign({}, {departureTime: departuresHour[index]}, {departureStation: departuresStation[index]},
                    {arrivalTime: arrivalsHour[index]}, {arrivalStation: arrivalsStation[index]}, {type: trainTypes[index]}, {number: trainNumbers[index]}))
            }
        }
        const totalPrice = this.$('table.total-amount').find('td.very-important').get()
            .map(it => it.children[0].data.trim())
            .map(it => it.replace(' €', '').replace(',','.'))
            .map(it => parseFloat(it));
        const dates = this.datesExtractor.extract();
        const travelWays = this.$('#block-command').find('.product-details').find('.travel-way').get();
        const roundTrips = []
        if (travelWays.length <= dates.length) {
            for(let index = 0; index < travelWays.length; index++) {
                roundTrips.push({
                    type: travelWays[index].children[0].data.trim(),
                    date: dates[index],
                    trains: [trains[index]]
                })
            }
        }
        return {
            trips:{
                code: parsedQuery.pnrRef,
                name: parsedQuery.ownerName,
                details: {
                    price: totalPrice.length === 1 ? totalPrice[0] : -1,
                    roundTrips: roundTrips
                }
            }
        }
    }

}