export class ResultExtractor {
    constructor(tripsExtractor, customExtractor) {
        this.tripsExtractor = tripsExtractor;
        this.customExtractor = customExtractor;
    }

    extract() {
        return {
            results: {
                ...this.tripsExtractor.extract(),
                ...this.customExtractor.extract()
            }
        }
    }
}