export class CustomExtractor {
    constructor($) {
        this.$ = $
    }

    extract() {
        const prices = this.$('#block-command').find('.product-header').find('td:last-child').get().map(it => it.children[0].data.trim())
            .map(it => it.replace(' €', '').replace(',', '.'))
            .map(it => parseFloat(it))
            .map(it => {
                return {
                    value: it
                }
            });
        return {
            custom: {
                prices: prices
            }
        }
    }
}