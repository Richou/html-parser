import log4js from 'log4js';
import moment from 'moment';

export class DatesExtractor {

    constructor($) {
        this.logger = log4js.getLogger('HtmlParser');
        this.logger.level = process.env.APPLICATION_LOG_LEVEL;
        this.$ = $
    }

    extract() {
        const dates = this.$('#block-travel').find('.pnr-summary').get().map(it => {
            const dates = it.children[0].data.trim().split('-')[1].trim();
            const startend = dates.split('et');
            return [...startend];
        }).reduce((accumulator, item) => [...accumulator, ...item],[])
        .map(it => it.replace('les', '').trim())
        .map(it => moment(it, 'DD/MM/YYYY').toDate())
        .map(it => moment(it).format('YYYY-MM-DD HH:mm:SSZ'));
        return dates;
    }

    _getBlock() {
        return ;
    }

}