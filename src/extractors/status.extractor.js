export class StatusExtractor {
    constructor($) {
        this.$ = $
    }

    extract() {
        const title = this.$('title').text();
        const status = title !== null && title !== undefined && title.toLowerCase() === 'confirmation de votre commande' ? "ok" : "ko"
        return {
            status
        }
    }
}