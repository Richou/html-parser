import cheerio from 'cheerio';
import fs from 'fs';
import os from 'os';
import log4js from 'log4js';
import Extracts from './extracts';

class HtmlParser {

    constructor(filePath) {
        this.logger = log4js.getLogger('HtmlParser');
        this.logger.level = process.env.APPLICATION_LOG_LEVEL;
        this.filePath = filePath;
    }

    process() {
        if (!fs.existsSync(`${this.filePath}/test.stripped.html`)) {
            this.logger.debug('Stripped file does not exists, creating it ...');
            this._stripFile();
        }
        const html = this._loadCheerio();
        const extracts = new Extracts(html);
        const result = JSON.stringify(extracts.process(), null, 2);
        fs.writeFileSync(`${this.filePath}/test-result.json`, result, {encoding: "utf8"});
    }

    _stripFile() {
        const fileRead = fs.readFileSync(`${this.filePath}/test.html`);
        const stripped = fileRead.toString().replace(/\\r\\n/gm, os.EOL);
        const antislashStripped = stripped.replace(/\\/gm, '');
        fs.writeFileSync(`${this.filePath}/test.stripped.html`, antislashStripped, 'utf8');
    }

    _loadCheerio() {
        return cheerio.load(fs.readFileSync(`${this.filePath}/test.stripped.html`));
    }
}

module.exports = HtmlParser;