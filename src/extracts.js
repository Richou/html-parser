import { DatesExtractor, ResultExtractor, StatusExtractor, TripsExtractor, CustomExtractor } from './extractors';

export default class Extracts {
    constructor($) {
        this.$ = $;
    }

    process() {
        const statusExtractor = new StatusExtractor(this.$);
        const datesExtractor = new DatesExtractor(this.$);
        const tripsExtractor = new TripsExtractor(this.$, datesExtractor);
        const customExtractor = new CustomExtractor(this.$);
        const resultExtractor = new ResultExtractor(tripsExtractor, customExtractor);
        return {
            ...statusExtractor.extract(),
            ...resultExtractor.extract()
        }
    }
}